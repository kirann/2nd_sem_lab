# Project done in lab for data structure and algorithms 
# by rajini man :)

#Lab1 (Stack)


#Whats done in the file

first i made a linkedList which will contain nodes pointing to the reference of next next node.
there is addFront and removeFront function which will add and remove first node in the chain with complexity O(1)

and in stack class there is an object of linked list as data member,

in pop function program will get the front data of the linked list which will and remove that data from linked list and return it
in push function program add the data to front of linked list
in isEmpty there is condition to check the counter
in top, program will return the front data of linked list.
in size, program will return the total count of nodes of linked list.

## this all thing is made without any loop being used so it have highest efficiency and O(1) complexity