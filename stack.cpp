#include <iostream>
using namespace std;
#include "linked_list.cpp"

template<class T>
class Stack{
  LinkedList <T> list;

  public:
    void push(T data){
        list.addFront(data);
    }

    T pop(){
        T temp=list.getFront();
        list.removeFront();
        return temp;
    }

    int size(){
        return list.getSize();
    }

    bool isEmpty(){
        return (list.getSize()==0) ? true : false;
    }

    T top(){
        return list.getFront();
    }
};



int main(){
    Stack<int> s1;
    cout<<"Empty : "<<s1.isEmpty()<<endl;
    s1.push(5);
    s1.push(4);
    s1.push(2);
    s1.push(8);
    cout<<"Top : "<<s1.top()<<endl;
    cout<<"Size : "<<s1.size()<<endl;
    cout<<"Pop : "<<s1.pop()<<endl;
    cout<<"Size : "<<s1.size()<<endl;
    cout<<"Empty : "<<s1.isEmpty()<<endl;
    return 0;
}