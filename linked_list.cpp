#include <iostream>
using namespace std;

template <class T>
class Node{
    public:
        T data;
        Node *next;
        Node(){}
        Node(T _data){
            data=_data;
        }
};

template <class T>
class LinkedList{
    Node <T> *head;
    Node <T> *tail;
    int count;

    public:
        LinkedList(){
            head=new Node<T>;
            tail=new Node<T>;
            count=0;
        }

        int getSize(){
            return count;
        }

        void addFront(T data){
            Node<T> *newNode = new Node<T>(data);
            if(count==0){
                newNode->next=NULL;
                head->next=newNode;
                tail->next=newNode;
            }else{
                newNode->next=head->next;
                head->next=newNode;
            }
            count++;
        }

        void displayX(){
            Node<T> *node=head;
            while(node->next){
                cout<<node->next->data<<endl;
                node=node->next;
            }
        }

        void removeFront(){
            if(count==0) return;
            Node<T> *temp=head->next->next;
            delete head->next;
            head->next=temp;
            count--;
        }

        T getFront(){
            return head->next->data;
        }
};

